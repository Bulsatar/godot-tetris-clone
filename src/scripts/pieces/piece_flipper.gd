extends piece_base
class_name piece_flipper

func rotate_piece(boundary,playedpieces):
	if(!movable):return
	if(self.rotation_degrees == 0): self.rotation_degrees = 90
	elif(self.rotation_degrees == 90): self.rotation_degrees = 0
	
	var rect = get_boundary()
	if(!boundary.encloses(rect) || IntersectsPlayedPieces(playedpieces)):
		if(self.rotation_degrees == 0): self.rotation_degrees = 90
		elif(self.rotation_degrees == 90): self.rotation_degrees = 0


