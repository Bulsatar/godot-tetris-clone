extends Node2D
class_name block_base

var deleted = false
var moverows = 0
var blocksize = 50
var label = null
var origmodulate = null

func _ready():
	deleted = false
	var globals = get_node("/root/Globals")
	blocksize = snapped(globals.blocksize,10)
	
	label = Label.new()
	self.add_child(label)
	label.size.x = blocksize
	label.size.y = blocksize
	origmodulate = self.modulate
	
func _process(delta):  
	var parent_rotation = get_parent().rotation 
	set_rotation(-parent_rotation)
	self.rotation_degrees = snapped(self.rotation_degrees,10)
	
func set_rgb(rgb):
	if(rgb == null):
		rgb = origmodulate
	self.modulate = rgb
	
func set_deleted():
	deleted = true
	self.visible = false

func is_deleted():
	return deleted
	
func get_rect()-> Rect2:
	var x = snapped(self.global_position.x,10)
	var y = snapped(self.global_position.y,10)
	
	return Rect2(x, y,blocksize,blocksize)
	
func move_down():
	for row in moverows:
		var y = self.global_position.y + blocksize
		var x = self.global_position.x 
		self.position = Vector2(snapped(x,10), snapped(y,10))
	moverows = 0
