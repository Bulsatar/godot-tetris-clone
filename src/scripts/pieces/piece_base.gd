extends Node2D
class_name piece_base

var blocks = []
var movable = false
var blocksize = 50
var type = ""

func _ready():
	for child in get_children():
		if child is Sprite2D:
			blocks.append(child)
	movable = true
	var globals = get_node("/root/Globals")
	blocksize = globals.blocksize

func color_blocks(rgb_color):
	for block in blocks:
		block.set_rgb(rgb_color)
func setType(piecetype):
	type = piecetype
func getType():
	return type

func move_down(boundary,playedblocks)-> bool:
	if(!movable): return false
	var success = false
	var rect = get_boundary()
	var y = rect.position.y + blocksize
	y = int(snapped(y,10))
	rect.position.y = y
	var isenclosed = boundary.encloses(rect)
	var doesintersect = WouldIntersect(playedblocks,snapped(0,10),snapped(blocksize,10))
	if(isenclosed && !doesintersect):
		y = self.position.y + blocksize
		y = int(snapped(y,10))
		self.position = Vector2(self.position.x, y)
		success = true

	return success

func move_left(boundary,playedblocks):
	if(!movable):return
	var rect = get_boundary()
	rect.position.x -= blocksize
	var isenclosed = boundary.encloses(rect)
	var doesintersect = WouldIntersect(playedblocks,-blocksize,0)
	if(isenclosed && !doesintersect):
		self.position = Vector2(self.position.x - blocksize, self.position.y)
		
func move_right(boundary,playedblocks):
	if(!movable):return
	var rect = get_boundary()
	rect.position.x += blocksize
	var isenclosed = boundary.encloses(rect)
	var doesintersect = WouldIntersect(playedblocks,blocksize,0)
	if(isenclosed && !doesintersect):
		self.position = Vector2(self.position.x + blocksize, self.position.y)

func rotate_piece(boundary,playedblocks):
	if(!movable):return
	self.rotation_degrees += snapped(90,10)
	var isenclosed = boundary.encloses(get_boundary())
	var doesintersect = IntersectsPlayedPieces(playedblocks)
	if(!isenclosed || doesintersect):
		self.rotation_degrees -= snapped(90,10)

func WouldIntersect(playedblocks,x,y) -> bool:
	var intersects = false
	for setblock in playedblocks:
		if(!setblock.is_deleted()):
			var setrect = setblock.get_rect()
			for currblock in blocks:				
				var currrect = currblock.get_rect()
				currrect.position.x += x
				currrect.position.y += y
				if(setrect.intersects(currrect,false)):
					intersects = true
		
	return intersects
	
func IntersectsPlayedPieces(playedblocks)-> bool:
	var intersects = false
	for setblock in playedblocks:		
		if(!setblock.is_deleted()):
			var setrect = setblock.get_rect()
			for currblock in blocks:
				var currrect = currblock.get_rect()
				if(setrect.intersects(currrect,false)):
					intersects = true
	return intersects

func get_boundary() -> Rect2:
	return Rect2(get_left_boundary()
		,get_top_boundary()
		,get_right_boundary() - get_left_boundary()
		,get_bottom_boundary() - get_top_boundary())
		
func get_top_boundary():
	var y = 1000000 #stupid high to accomodate min
	for block in blocks:
		if(!block.is_deleted()):
			y = min(y,block.global_position.y)
	return int(round(y))

func get_bottom_boundary():
	var y = 0
	for block in blocks:
		if(!block.is_deleted()):
			y = max(y,block.global_position.y)
	return int(round(y + blocksize)) #accomodate block height

func get_left_boundary():
	var x = 1000000 #stupid high to accomodate min
	for block in blocks:
		if(!block.is_deleted()):
			x = min(x, block.global_position.x )
	return int(round(x))

func get_right_boundary():
	var x = 0
	for block in blocks:
		if(!block.is_deleted()):
			x = max(x, block.global_position.x)
	return int(round(x + blocksize)) #accomodate block width
	
func get_blocks():
	return blocks
	
func is_movable():
	return movable
	
func freeze():
	movable = false
