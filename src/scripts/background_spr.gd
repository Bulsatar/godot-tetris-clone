extends Sprite2D
class_name bg_sprite

var speed = 5.0
var xdirection = 1
var ydirection = 1
var boundary = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _setup(cameraboundary):
	boundary = cameraboundary
func switch_texture(texture):
	self.set_texture(texture)
	
func _process(delta):
	self.position.x += (speed * xdirection) * delta
	self.position.y += (speed * ydirection) * delta
	var g_pos_x = self.global_position.x 
	var g_pos_y = self.global_position.y
	var rect = self.get_rect()
	var g_rect = Rect2(g_pos_x, g_pos_y,rect.size.x, rect.size.y)
	if(!g_rect.encloses(boundary)):
		if(g_pos_x  + rect.size.x < boundary.position.x + boundary.size.x): xdirection = 1
		if(g_pos_x > boundary.position.x): xdirection = -1
		if(g_pos_y + rect.size.y < boundary.position.y + boundary.size.y): ydirection = 1
		if(g_pos_y > boundary.position.y): ydirection = -1
