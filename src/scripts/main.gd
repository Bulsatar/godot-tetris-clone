extends Node2D

var current_piece = null
var upcoming_piece = null
var swap_piece = null
var settledblocks = []

var vert_speedlimit = 100
var vert_cyclecount = 0
var orig_vert_speed = 100
var hoz_speedlimit = 45
var hoz_left_cyclecount = 0
var hoz_right_cyclecount = 0
var inputhandler = null

var playboundary = Rect2(0,0,500,1000)
var blocksize = 50
var current_theme_name = "cherry"
var current_theme = null
var bgspr = null
var camrect = null
var bg = null

var gamestarted = false
var gameover = false
var gamepaused = false
var current_level = 1
var clearedlines = 0

var linesclearedlabel = null
var levellabel = null
var globals = null
var debugbox = null

# Called when the node enters the scene tree for the first time.
func _ready():
	globals = get_node("/root/Globals")
	blocksize = globals.blocksize
	debugbox = get_node("debug_label")
	linesclearedlabel = get_node("LinesCleared_Label")
	levellabel = get_node("Level_Label")
	levellabel.text = str("LEVEL: ", current_level)
	var themes = globals.themes
	current_theme = themes[current_theme_name]
	inputhandler = InputHandler.new()
	inputhandler._setup(self)
	SetLinesCleared()
	
	#background switching should be moved
	bgspr = get_node("background_spr")
	var cam = get_node("Camera2D")
	var vp_rect = get_viewport_rect()
	var cam_pos_x = vp_rect.position.x + cam.offset.x
	var cam_pos_y = vp_rect.position.y + cam.offset.y
	var cam_size_x = vp_rect.size.x / cam.zoom.x
	var cam_size_y = vp_rect.size.y / cam.zoom.y
	camrect = Rect2(cam_pos_x, cam_pos_y, cam_size_x, cam_size_y)
	bg = globals.backgrounds[current_theme_name].pick_random()
	bgspr._setup(camrect)	
	bgspr.switch_texture(bg)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	if(!gamepaused):
		if(gamestarted && !gameover):
			DropPiece()
			inputhandler.HandlePlayerContinuousMovement()
		if(gameover):
			GameOver()
		
	setdebugbox()

func _input(event):
	inputhandler.Handle_Event(event)
	

func StartGame():
	if(!gamestarted && !gameover):
		SetCurrentPiece(true)
		SetUpcomingPiece()
		StartCurrentPiece()
		gamestarted = true

func RestartGame():
	if(gamestarted && gameover):
		return

func GameOver():
	current_piece.freeze()


func GetPiece():
	var newpiece = globals.GetRandomTetromino()
	add_child(newpiece)
	var type = newpiece.getType()
	var color = current_theme[type]
	newpiece.color_blocks(color)
	return newpiece

func SetLinesCleared():
	linesclearedlabel.text = str("CLEARED  ", clearedlines)
	

func SetUpcomingPiece():
	upcoming_piece =  GetPiece()
	upcoming_piece.position = Vector2(550,350)
	upcoming_piece.scale.x = 0.60
	upcoming_piece.scale.y = 0.60

func SetCurrentPiece(isnewgame):
	if(isnewgame):
		current_piece =  GetPiece()
	else:
		current_piece = upcoming_piece
		current_piece.scale.x = 1
		current_piece.scale.y = 1
		
func SwapPieces():	
	var x = current_piece.global_position.x
	var y = current_piece.global_position.y
	if(swap_piece == null):		
		swap_piece = current_piece
		current_piece = upcoming_piece
		SetUpcomingPiece()
	else:
		var temppiece = current_piece
		current_piece = swap_piece
		swap_piece = temppiece
	
	current_piece.global_position.x = snapped(x,10)
	current_piece.global_position.y = snapped(y,10)
	current_piece.scale.x = 1
	current_piece.scale.y = 1
	
	swap_piece.position = Vector2(550,-100)
	swap_piece.scale.x = 0.60
	swap_piece.scale.y = 0.60
	
	#have to make sure a swap doesn't land a piece out of bounds or conflicts
	#do not allow the swap if it does
	var outofbounds = !playboundary.encloses(current_piece.get_boundary())
	var hitsotherpieces = current_piece.IntersectsPlayedPieces(settledblocks)
	if(outofbounds || hitsotherpieces):
		SwapPieces()
	
		

func StartCurrentPiece():
	#initialize piece to center top before adjustment
	current_piece.position = Vector2(250,0)
	var y = int(current_piece.get_top_boundary() * int(-1))
	current_piece.position = Vector2(250,y)
	if(current_piece.IntersectsPlayedPieces(settledblocks)):
		gameover = true

func LockCurrentPiece():
	current_piece.freeze()
	#reset vert_speedlimit if they were holding down
	vert_speedlimit = orig_vert_speed
	CacheCurrentPieceBlocks()
	SetCurrentPiece(false)
	ProcessLines()
	IncreaseLevel()
	SetUpcomingPiece()
	StartCurrentPiece()
	

func CacheCurrentPieceBlocks():
	for block in current_piece.get_blocks():
		var x = block.global_position.x
		var y = block.global_position.y
		current_piece.remove_child(block)
		add_child(block)
		block.position = Vector2(snapped(x,10),int(y))
		settledblocks.append(block)
	
func DropPiece():
	vert_cyclecount += 1
	if(vert_cyclecount >= vert_speedlimit):
		var success = current_piece.move_down(playboundary,settledblocks)
		vert_cyclecount = 0
		HandleDrop(success)

func IncreaseLevel():
	if(clearedlines >= current_level *10):
		if(vert_speedlimit <= 10):
			vert_speedlimit = max(vert_speedlimit-2, 1)
		else:
			vert_speedlimit = max(vert_speedlimit-10, 3) 
		orig_vert_speed = vert_speedlimit #just in case
		current_level += 1
	levellabel.text = str("LEVEL: ", current_level)
	
		
	
func ProcessLines():
	var rowstarty = playboundary.size.y
	var rowblocks = []
	while rowstarty >= 0:
		for block in settledblocks:
			var y = block.global_position.y 
			if(!block.is_deleted() && y == rowstarty):
				rowblocks.append(block)
		var size = rowblocks.size()
		if(size == 10):
			ClearLine(rowblocks,rowstarty)
		rowstarty -= blocksize
		rowblocks = []
	DropBlocks()
	SetLinesCleared()

func DropBlocks():
	for block in settledblocks:
		block.move_down()
	
func ClearLine(rowblocks,rowy):
	clearedlines += 1
	for block in rowblocks:
		block.set_deleted()
	for block in settledblocks:
		var isdeleted = block.is_deleted()
		if(!isdeleted && block.global_position.y < rowy):
			block.moverows += 1
	
	
	
func HandleDrop(success):
	if(!success):
		LockCurrentPiece()

	
func setdebugbox():
	var boxtext = str("vert_speedlimit ",vert_speedlimit,"\norig vert_speedlimit: ", orig_vert_speed)
	#debugbox.text = boxtext
