extends Node
class_name InputHandler

var _main = null
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _setup(main):
	_main = main

func Handle_Event(event):
	if(event.is_action_pressed("ui_select")):
		_main.gamepaused = !_main.gamepaused
	#todo handle game paused activity. but for now just stop all actions
	if(_main.gamepaused):	return
	
	if event is InputEventKey:
		if (event.pressed && !_main.gamestarted):
			_main.StartGame()
			return
			
	if(event.is_action_released("ui_down")):
		_main.vert_speedlimit = _main.orig_vert_speed
	if(event.is_action_released("ui_left")):
		_main.hoz_left_cyclecount = 0
	if(event.is_action_released("ui_right")):
		_main.hoz_right_cyclecount = 0
	
	if(Input.is_action_pressed("ui_left")):
		_main.current_piece.move_left(_main.playboundary,_main.settledblocks)
	if(Input.is_action_pressed("ui_right")):
		_main.current_piece.move_right(_main.playboundary,_main.settledblocks)
	if(event.is_action_pressed("ui_up")):
		_main.current_piece.rotate_piece(_main.playboundary,_main.settledblocks)		
	if(event.is_action_pressed("ui_down")):
		_main.orig_vert_speed = _main.vert_speedlimit
		_main.vert_speedlimit = 3
	if(event.is_action_pressed("ui_end")):
		_main.SwapPieces()
		
func HandlePlayerContinuousMovement():
	#_input only fires once. to check every time, look during process
	if(Input.is_action_pressed("ui_left")):
		_main.hoz_left_cyclecount += 1
		if(_main.hoz_left_cyclecount >= _main.hoz_speedlimit):
			_main.current_piece.move_left(_main.playboundary,_main.settledblocks)
	if(Input.is_action_pressed("ui_right")):
		_main.hoz_right_cyclecount += 1
		if(_main.hoz_right_cyclecount >= _main.hoz_speedlimit):
			_main.current_piece.move_right(_main.playboundary,_main.settledblocks)
