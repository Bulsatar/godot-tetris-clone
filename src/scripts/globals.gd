extends Node

var blocksize = 50
var current_theme = "default"
var current_background = "cherry"


var piece_I = preload("res://scenes/piece_I.tscn")
var piece_J = preload("res://scenes/piece_J.tscn")
var piece_L = preload("res://scenes/piece_L.tscn")
var piece_O = preload("res://scenes/piece_O.tscn")
var piece_S = preload("res://scenes/piece_S.tscn")
var piece_Z = preload("res://scenes/piece_Z.tscn")
var piece_T = preload("res://scenes/piece_T.tscn")
var tetrisPieces = [piece_I, piece_J, piece_L, piece_O, piece_S, piece_Z, piece_T ]



func GetRandomTetromino():
	return tetrisPieces.pick_random().instantiate() #[randi() % tetrisPieces.size()]
	
var defaulttheme = {"I":Color.CYAN
	,"J":Color.BLUE
	,"L":Color.ORANGE
	,"O":Color.YELLOW
	,"S":Color.GREEN
	,"Z":Color.RED
	,"T":Color.PURPLE}
var mandotheme = {"I":Color.from_string("#588b8b",Color.CORNFLOWER_BLUE)
	,"J":Color.from_string("#ffffff",Color.CORNFLOWER_BLUE)
	,"L":Color.from_string("#ffd5c2",Color.CORNFLOWER_BLUE)
	,"O":Color.from_string("#f28f3b",Color.CORNFLOWER_BLUE)
	,"S":Color.from_string("#721415",Color.CORNFLOWER_BLUE)
	,"Z":Color.from_string("#93b7be",Color.CORNFLOWER_BLUE)
	,"T":Color.from_string("#c8553d",Color.CORNFLOWER_BLUE)
	}
var cherrytheme = {"I":Color.from_string("#c3c3e6",Color.CORNFLOWER_BLUE)
	,"J":Color.from_string("#b370b0",Color.CORNFLOWER_BLUE)
	,"L":Color.from_string("#bba0ca",Color.CORNFLOWER_BLUE)
	,"O":Color.from_string("#87255b",Color.CORNFLOWER_BLUE)
	,"S":Color.from_string("#bea8aa",Color.CORNFLOWER_BLUE)
	,"Z":Color.from_string("#9e9885",Color.CORNFLOWER_BLUE)
	,"T":Color.from_string("#7c7f65",Color.CORNFLOWER_BLUE)
	}
var themes = {"default":defaulttheme,"mando":mandotheme,"cherry":cherrytheme}

var bg_cherry = [preload("res://assets/backgrounds/cherry/cherry1.png")
,preload("res://assets/backgrounds/cherry/cherry2.png")
,preload("res://assets/backgrounds/cherry/cherry3.png")
,preload("res://assets/backgrounds/cherry/cherry4.png")]

var backgrounds = {"cherry":bg_cherry}















